<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('admin.categories.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.categories.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>'required',
            'slug' =>'required|unique:categories'
        ]);
        // $slug = strtolower( str_replace(' ','-',$request->input('name')) );  // slug banauna lowercase gareko ani space ma - haleko ani slug unique hunuparxa
        // $categories = Category::whereSlug($slug)->get();
        // // dd($categories);
        // if( $categories-> count() > 0){
        //     return redirect()->back()->withErrors(['slug must be unique']);
        // }

        $category = new Category();
        $category->name = $request->input('name');
        $category->desc = $request->input('description');
        $category->parent_id = $request->input('parent_id');
        $category->slug = $slug = $request->input('slug');
        $category->save();
        return redirect()->route('admin.categories.index');

        // $category = Category::create([
        //     'name' => $request->input('name'),
        //     'description' => $reuest->input('description'),
        //     'parent_id' => $request->input('parent_id')
        // ])
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $categories = Category::all();
        return view('admin.categories.edit', [
            'category'=> $category,
            'categories' => $categories
        ] );   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $request->validate([
            'name'=>'required',
            'slug' =>'required|unique:categories'
        ]);
        // $slug = strtolower( str_replace(' ','-',$request->input('name')) );  // slug banauna lowercase gareko ani space ma - haleko ani slug unique hunuparxa
        // $categories = Category::whereSlug($slug)->get();
        // // dd($categories);
        // if( $categories-> count() > 0){
        //     return redirect()->back()->withErrors(['slug must be unique']);
        // }

        $category->name = $request->input('name');
        $category->desc = $request->input('description');
        $category->parent_id = $request->input('parent_id');
        $category->slug = $slug = $request->input('slug');
        $category->save();
        return redirect()->route('admin.categories.index');

        // $category = Category::create([
        //     'name' => $request->input('name'),
        //     'description' => $reuest->input('description'),
        //     'parent_id' => $request->input('parent_id')
        // ])
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();
        return back();
    }
}
