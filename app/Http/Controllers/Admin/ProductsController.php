<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\Category;
use Intervention\Image\Facades\Image;


class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::latest()->get();
        return view('admin.products.index',['products' => $products] );
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.products.create', ['categories'=> $categories] );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request; /*data haru json format ma dekhauxa */
        $validated = $request->validate([
            'product_name' => 'required|max:100|unique:products',
            'product_desc' => 'required',
            'price' => 'required',
            'category_id' => 'required|integer|min:1',
        ],
        
            [
                'required' => 'This field is very much required to be fillled', /* required ko case ma yo msg dekhaune */
                'max' => 'this is too long',
                'product_desc.required' => 'Product desc should not be less than 100 words' , /*yo chai specific element ko name diyera */
                'category_id.required' => 'minimum one item need to be selected',
            ],
        
    );
   
        $product = new Product;
        $product->product_name = $request->input('product_name');
        $product->product_desc = $request->input('product_desc');
        $product->price = $request->input('price');
        $product->category_id = $request->input('category_id');
        if ($request->hasFile('image_upload')) {
            //uploading image to images folder
            $name = $request->file('image_upload')->getClientOriginalName();
            $request->file('image_upload')->storeAs('public/images',$name); 
            //croping the image and saving it to thumbnail folder inside images //
            // $image_resize =Image::make(storage_path('app/public/images/'.$name));
            // $image_resize->resize(550, 750);
            // $image_resize->save(storage_path('app/public/images/thumbnail/' .$name)); //images bata resize garisakera thumbnail ma rakhnne */
            $product->image = $name ; /*database ma save gareko name matra pass garera without passing whole path */
            // image_crop($name, 550, 750);
        }  
      
        if($product->save()){
            // return redirect()->route('/admin/products');/*save successful bhayo bhne route ma jane */
            return redirect()->route('admin.categories.index'); /*agadi route ma hamle rakheko name */
        }else {
            return redirect()->back(); /*save unsuccessful bhayo bhne return back */
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $categories = Category::all();
        return view('admin.products.edit', ['product'=> $product,'categories'=> $categories] );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Product $product) /*yo bhneko route ra model binf gareko */
    {
        //  $this->authorize('update', $product);
    //    return $request;
    // $products = Product::find($product->id);

    $validated = $request->validate([
        'product_name' => 'required|max:100|unique:products',
        'product_desc' => 'required',
        'price' => 'required|integer',
        'category_id' => 'required|integer|min:1',
    ],
        [
            'required' => 'this field shouldnot be empty',
            'max' => 'shouldnot be longer than 10 characters',
            'integer'=>'only numerical value',

        ], );


        $product = new Product;
        $product->product_name = $request->input('product_name');
        $product->product_desc = $request->input('product_desc');
        $product->price = $request->input('price');
        $product->category_id = $request->input('category_id');
        if ($request->hasFile('image_upload')) {
            // uploading image to images folder
            $name = $request->file('image_upload')->getClientOriginalName();
            $request->file('image_upload')->storeAs('public/images', $name);
            // croping the image and saving it to thumbnail folder inside images folder
            // $image_resize = Image::make(storage_path('app/public/images/'.$name));
            // $image_resize->resize(550, 750);
            // $image_resize->save(storage_path('app/public/images/thumbnail/'.$name));
            image_crop($name, 550, 750);
            $product->image = $name;
        }
        // return $product;
        if($product->save()){
            return redirect()->route('admin.products.index');
        }else {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
     public function destroy(Product $product)
    {
        $product->delete();
        return back();
    }
}
