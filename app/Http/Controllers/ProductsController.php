<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ProductsController extends Controller

{
    public function index(){    //yo function ho//
        $products = Product::latest()->get();
        return view('home', ['products' => $products] );
    }
    
    public function show(Product $product){
        return view('product', compact('product') );
    }
    
    public function search(){
        return 'this is search';
    }

    // public function index(){
    //     $products = Product::latest()->get();
    //     // $singleProduct = products::latest()->take(1)->get();
    //     $threeProduct = Product::latest()->skip(1)->take(3)->get();
    //     $twoProduct = Product::latest()->skip(4)->take(2)->get();
	//     return view('home',['products'=>$products,'threeProduct'=>$threeProduct,'twoProduct'=>$twoProduct]);
    // }
    // public function show(Product $product){
    //     return view('product',compact('product') );
    // }
    // public function search(){
        
    // }
    
    
}