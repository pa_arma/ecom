<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class SearchController extends Controller
{
    public function search(){
        // return request(['search','category']);
        $products = Product::latest()->search(request(['search', 'category']))->get(); //yo array pass gareko ho request garera ,hamle roduct model lai call garira chau so function create garxau model bhitrako product.php mA//
        return view('products', ['products' => $products]); //yo garda products.blade.php ko view page dekhauxa //

    }
}
//in dbms esto hunxa select * from products where product_name like '%mobile%' or product_desc like '%mobile%'