<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    public function products(){
        return $this->hasMany(Product::class);
    }

    // public function scopeSearch($query, array $terms)
    // {
       
    //     if ( $terms('search') !== '' ) {
    //         $query->where('product_name', 'like', '%'. request('search') .'%') //product name ko adhar ma search garxa//
    //         ->orWhere('product_desc', 'like', '%'. request('search') .'%'); //product desc ko adhar ma search garxa // 
    //      }
    //      return $query;
    // }
}
