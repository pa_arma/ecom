<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $fillable = [
        'product_name',
        'product_desc',
        'image',
        'price',
        'category_id'
    ];

    protected $attributes = [
        'image' => '',
    ];
    protected $with =['category'];

    public function category()/*here laravel will undersatnd category means category_id*/
    {
        //hasOne , hasMany, belongsTo
       return $this ->belongsTo(Category::class);/*mathi category rakheko bhayera laravel le bujhxa category_id bhnera so ,category_id lekhirkhnu pardiana*/
    }
    public function scopeSearch($query, array $terms)  //hamle eta second argument pathayo array type ko terms bhnera //search controller ma array banaye paxi eta fuunction banako //query bydefault laravel nai pass gardinxa//
    {
        $search = $terms['search'];
        $category = $terms['category'];
        $query->when($search, function($query) use($search){
            return $query->where('product_name','like', '%'. $search .'%')
            ->orWhere('product_desc', 'like', '%'.$search .'%');
        }
        );
    $query->when($category, function($query, $category){
        return $query->whereCategoryId($category);
    });
       // This is another way instead of using when by using if//
        // if ( $terms['search'] !== '' ) //yedi search null haina bhne tala ko function run garauxa//
        // {
        //     $query->where('product_name', 'like', '%'. $terms['search'] .'%') //product name ko adhar ma search garxa//
        //     ->orWhere('product_desc', 'like', '%'. $terms['search'] .'%'); //product desc ko adhar ma search garxa // 
        // }
         return $query;
}
}
