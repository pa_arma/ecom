
<?php

use App\Models\Category;
use Intervention\Image\Facades\Image;

use function PHPUnit\Framework\fileExists;

if(!function_exists('image_crop')){
        function image_crop($image_name,$width,$height){
            if(
                fileExists(storage_path('app/public/images/'.$image_name)) &&
                fileExists(storage_path('app/public/images/thumbnail/'.$image_name)==false)
             ){
                $image_resize=Image::make(storage_path('app/public/images/'.$image_name));
                $image_resize->resize($width,$height);
                $image_resize->save(storage_path('app/public/images/thumbnail/'.$image_name));
            }
            // return asset('app/public/images/thumbnail/'.$image_name);
        }
        function cover_crop($image_name,$width,$height){
            if(
                fileExists(storage_path('app/public/images/'.$image_name)) &&
                fileExists(storage_path('app/public/images/cover/'.$image_name)==false)
             ){
                $image_resize=Image::make(storage_path('app/public/images/'.$image_name));
                $image_resize->resize($width,$height);
                $image_resize->save(storage_path('app/public/images/cover/'.$image_name));
            }
            // return asset('app/public/images/thumbnail/'.$image_name);
        }
        function medium_crop($image_name,$width,$height){
            if(
                fileExists(storage_path('app/public/images/'.$image_name)) &&
                fileExists(storage_path('app/public/images/medium/'.$image_name)==false)
             ){
                $image_resize=Image::make(storage_path('app/public/images/'.$image_name));
                $image_resize->resize($width,$height);
                $image_resize->save(storage_path('app/public/images/medium/'.$image_name));
            }
            // return asset('app/public/images/thumbnail/'.$image_name);
        }
    }
    //yo tala ko category search garna ko lagi ho , helper ma rakhyo bhne ja bata ni call garna milxa //
if(!function_exists('categories_list')){
    function categories_list(){
        return category::whereParentId(0)->get(); //top level ko link lai parent bata gareko//
        // return Category::where('parent_id, 0')->get();
    }
}



// <!-- // if(!function_exists('image_crop')){
//     function image_crop( $image_name, $width = 550, $height= 750 ){
//         if(
//             file_exists(storage_path('app/public/images/'.$image_name))
//             && !file_exists(storage_path('app/public/images/thumbnail/'.$image_name))
//             ){ //yedi storage path ma image exit garxa tara thumbnail ma tyo image ko thumbnail chaina bhne matra talako process use garera crop garera thumbnail ma pathauxa*//
//             $image_resize =Image::make(storage_path('app/public/images/'.$image_name));
//             $image_resize->resize($width, $height);
//             $image_resize->save(storage_path('app/public/images/thumbnail/' .$image_name)); //images bata resize garisakera thumbnail ma rakhnne */
//         }
//         return asset('storage/images/thumbnail/'.$image_name);
//     }
// } -->