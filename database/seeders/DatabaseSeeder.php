<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;
use App\Models\Category;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Product::truncate();
        // Category::truncate();
        //  \App\Models\User::factory(10)->create();
        $category = \App\Models\Category::create([
            'name' => 'Accessories',
            'desc' => 'these are latest mobile'
          
        ]);
        Product::factory(5)->create([
            'category_id'=> 3
        ]);






        //  $category =\App\Models\Category::create([
        //      'name' => 'cheaper Mobile',
        //      'desc' => 'This category contains cheapest mobiles'
        //  ]);

        //  Product::create([
        //      'product_name'=> 'samsung',
        //      'product_desc'=>'This is an amazing iphone',
        //      'category_id' => $category->id,
        //      'price' =>'2300000',
        //      'image' => ''
        //  ]);

    }
}
