<x-admin.layout>
    <div class="az-content az-content-dashboard">
        <div class="container">
          <div class="az-content-body">
            <h2>Create Category</h2>
            <form action="{{ route('admin.categories.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                {{-- <x-forms.input type="text" name='full_name'/>    esma j j atrributr rakhyo,tyo input.blade ma change garna milxa  --}}
                Category Name : <input type="text" name="name" id="name" class="form-control" value="{{ old('name') }}" 
                @error('name')
                    style="border-color: red;"
                @enderror
                >
                Category Slug :<input type="text" name="slug" id="slug" class="form-control" value="{{ old('name') }}"><br><br>
                Category Description: <textarea name="description" id="" cols="30" rows="10" class="form-control">{{ old('description') }}</textarea><br>
                Parent Category: 
                <x-forms.select name="parent_id" class="form-control">
                <option value='0'>Select</option>
                @foreach ($categories as $category)
                    <option value= "{{ $category->id }}"  {{ $category->id == old('parent_id') ? "selected": '' }}>{{$category->name}}</option>
                @endforeach   
                
                @error('category_id')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror   

                <input type="file" name="image_file" id="">
            </x-forms.select> <br>
                 <input type="submit" name="Submit" value="save" class="form-control">
            </form>
          </div>
        </div>
    </div>
</x-admin.layout>
<script>
    JQuert(document).ready(function($){
        $('#name').on('change'function(){
          var name = $('#name'.val());
          var slug = name.toLowerCase();  //slug lai lowercase ma lageko //
          var slug = name.replace(/\s+/g, '-'); //regular expression use garera slug ko space nikalera dash ma convert gareko//
          $('#slug').val(slug); //yesle category name ma rakheko name lai slug ko part ma lower case ma lekhdinxa//

        })
    })
    </script>
