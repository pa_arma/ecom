<x-admin.layout>
    <div class="az-content az-content-dashboard">
        <div class="container">
          <div class="az-content-body">
            <h2>Create Product</h2>
            <form action="{{ route('admin.products.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                {{-- <x-forms.input type="text" name='full_name'/>    esma j j atrributr rakhyo,tyo input.blade ma change garna milxa  --}}
                Product Name : <input type="text" name="product_name" id="" class="form-control" value="{{ old('product_name') }}" 
                @error('product_name')
                    style="border-color: red;"
                @enderror ><br>
                @error('product_name')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
               
                Product Description: <textarea name="product_desc" id="" cols="30" rows="10" class="form-control">{{ old('product_desc') }}</textarea><br>
                @error('product_desc')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror

                Price: <input type="text" name="price" id="" class="form-control" value=" {{ old('price') }}"><br>
                @error('price')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror

                Category: 
                <x-forms.select name="category_id" class="form-control">
                <option value='0'>Select</option>
                @foreach ($categories as $category)
                    <option value= "{{ $category->id }}" @if ($category->id == old('category_id')) selected @endif>{{$category->name}}</option>
                @endforeach   
                
                @error('category_id')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror   

                <input type="file" name="image_upload" id="">
            </x-forms.select> <br>
                 <input type="submit" name="Submit" value="save" class="form-control">
            </form>
          </div>
        </div>
    </div>
</x-admin.layout>