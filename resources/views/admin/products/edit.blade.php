<x-admin.layout>
    <div class="az-content az-content-dashboard">
        <div class="container">
          <div class="az-content-body">
              <h2>Update Product: {{ $product->product_name}}</h2>
              <form action="{{route('admin.products.update', $product->id)}}" method="POST">
                @method('PUT') {{--ROUTE le update ko lagi PUT/PATCH dekha cha when u see in rote list but form ma POST matra rakhna milxa so  here @method garera PUT rakhne--}}
                @csrf
                {{-- <x-forms.input type="text" name='full_name'/>    esma j j atrributr rakhyo,tyo input.blade ma change garna milxa  --}}
                Product Name : <input type="text" name="product_name" id="" class="form-control" value="{{ $product->product_name}}"><br>
                {{-- @error('product_name')  <div class="alert alert-danger">{{ $message }}</div>  @enderror --}}

                Product Description: <textarea name="product_desc" id="" cols="30" rows="10" class="form-control">{{ $product->product_desc}}</textarea><br>
                {{-- @error('product_desc')  <div class="alert alert-danger">{{ $message }}</div>  @enderror --}}

                Price: <input type="text" name="price" id="" class="form-control" value="{{ $product->price}}"><br>
                {{-- @error('price')  <div class="alert alert-danger">{{ $message }}</div>  @enderror --}}
                Category: 
                <x-forms.select name="category_id" class="form-control">
                    <option value='0'>Select</option>
                    @foreach ($categories as $category)
                        <option value= "{{ $category->id }}" @if ($category->id == $product->category_id) selected @endif>{{$category->name}}</option>
                        {{-- <option value= "{{ $category->id }}" {{$category->id == $product->category_id ? "selected" : '' }}>{{$category->name}}</option> mathi ko ra yo same ho --}}
                    @endforeach
                </x-forms.select><br>

                {{-- <select name="category_id" id=""><option value='0'>Select</option>
                    @foreach ($categories as $category)
                    <option value= "{{ $category->id }}">{{$category->name}}</option>
                    @endforeach
                    </select><br> --}}
                    <input type="submit" name="Submit" value="Save" class="form-control">
            </form>
           
          </div>
        </div>
    </div>
</x-admin.layout>