<div class="search-bar-top">
    <div class="search-bar">
        @php
        $categories = categories_list();
        @endphp{{--categories_list bhnne function call gareko --}} 
        <form method="GET" action="{{ route('search') }}">
            <select name="category">
                <option selected="selected" value="">All Category</option>
                {{-- <option>watch</option>
                <option>mobile</option>
                <option>kid’s item</option> --}}
                @foreach ($categories as $category)
                   <option value="{{ $category->id }}">{{ $category->name }}</option> 
                @endforeach
            </select>
            <input name="search" value="{{request('search')}}" placeholder="Search Products Right Here....." type="search">
            <button class="btnn"><i class="ti-search"></i></button>
        </form>
    </div>
</div>