@extends('product-layout')
@section('menu')
    @include('includes/menu')
@endsection

@section('content')
    <section class="hero-slider">
    <div class="container">
        <div class="single-slider" style="background-image: url('/storage/images/{{$product->image }}')">
            <div class="row">
                <div class="col-lg-4 col-md-6 col-12">
                    <h2>{{ $product->product_name }}</h2>
                     <p>{{ $product->product_desc}}</p>
                    
                </div>
            </div>
        </div>
        </div>
    </section>
    <a href="/">Go to Homeeeee</a>
@endsection