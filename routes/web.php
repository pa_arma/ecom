<?php

use Illuminate\Support\Facades\Route;
use App\Models\Product;
use App\Models\Category;
use App\Http\Controllers\Admin\CategoriesController;
use App\Http\Controllers\Admin\ProductsController;
use App\Http\Controllers\ProductsController as PC;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\OrderItemController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can ash web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/admin/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';


Route::get('/', function () {
    $products = Product::latest()->get();/*eta Product::latest()->with('category')->get() ; with use bhako cha because hamle Product bhnee model ma define gareko bhayera tara if with use garna pardaina bhne without use garna sakinxa like this: Product::latest()->without('category')->get(); */ 
    return view('home',['products'=>$products] );
});

Route::get('/dashboard',[ProductsController::class,'index'])->name('home');
// Route::get('/products/search', [ProductsController::class, 'search'])->name('products.search'); //first ma ProductsController ma search function banaune then web.php ma route banaune//
Route::resource('/products',PC::class)->only(['index','show']); //esari resurces ko index ra show matra dekhau bhnna milxa //
Route::get('search',[SearchController::class, 'search'])->name('search');

Route::get('/categories/{category}',function(Category $category){
    // $products = Product::whereCategoryId($category->id)->get();
    $products = $category->products;
    return view('category',['products' => $products, 'category' => $category]);
});/*yo garda tyo category ma bhako sabai products haru dekhauxa */
/*yo garda products haru ko category call hunxa*/

Route::resource('order',OrderController::class);
Route::post('cart',[OrderItemController::class, 'store'])->name('add_to_cart')->middleware('auth'); //cart ma add garna agadi logged in hunuparxa //

//admin routing
/*yo group bhitra ko sabai component lai authentication chaixa bhneko */
Route::middleware(['auth'])->prefix('admin')->name('admin.')->group(function () {
    Route::get('/dashboard', [DashboardController::class,'index'])->name('dashboard');  /*dashboard ma pugna pani authorization chiaxa */
    // Route::get('/admin/products', [ProductsController::class,'index'])->name('products_list');/*product_list bhnne name diyo bhne routing sajilo hunxa */
    // Route::get('/admin/products/create', [ProductsController::class,'create'])->name('create_product');
    // Route::post('/admin/products/store', [ProductsController::class, 'store']);
    // Route::get('/admin/products/edit/{product}', [ProductsController::class, 'edit']);
     Route::resource('products',ProductsController::class);
     Route::resource('categories',CategoriesController::class);
});